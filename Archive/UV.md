# UV

**Ultraviolet (UV)** is **electromagnetic radiation** with wavelength from 10 nm to 400 nm, present in sunlight.
It is also produced by electric arcs and specialized lights, such as mercury-vapor lamps, or black lights.
Although long-wavelength ultraviolet is not considered an ionizing radiation because its photons lack the energy to ionize atoms, it can cause **chemical reactions** and causes many substances to glow or **fluoresce**. 

Artificially, UV light is usually produced with mercury-vapour lamps.
The trouble is that these lamps also produce a certain visible light content.
However, one can make 'pure' UV of a single wavelength using a laser.
Usually a frequency-doubled argon gas is used to produce a continuous wave beam of UV light.

Generally, if a light source ('the atom') is in an excited state with energy E, it may spontaneously decay to a lower lying level (e.g., the ground state) with energy e, releasing the difference in energy between the two states (E-e) as a photon.
If the difference is energy is just right (between 3.10 eV and 124 eV, the photon produced has the right wavelength (10 nm ~ 400 nm).

## Producing UV

### Lamps

Either Xenon-mercury or blacklight is what we need, alternatives include xenon, mercury, and tanning lamps.

##### Xenon-Mercury Lamp

In xenon-mercury short-arc lamps, the majority of the light is generated in a pinpoint-sized cloud of plasma situated at the tip of each electrode.
The light generation volume is shaped like two intersecting cones, and the luminous intensity falls off exponentially moving towards the centre of the lamp.
Xenon-mercury short-arc lamps have a bluish-white spectrum and extremely high UV output.

The very small size of the arc makes it possible to focus the light from the lamp with moderate precision.
For increased precision, one could use single mode laser diodes and white light supercontinuum lasers that can produce diffraction-limited spot.

All xenon short-arc lamps generate substantial ultraviolet radiation.
Xenon has strong spectral lines in the UV bands, and these readily pass through the fused quartz lamp envelope.
They are also inherently unstable, prone to phenomena such as plasma oscillation and thermal runaway.
Because of that, they require a proper power supply with moderately consistent output.

##### Blacklight

A blacklight emits long-wave (UV-A) ultraviolet light and very little visible light.
Although many other types of lamp emit ultraviolet light with visible light, black lights are essential when UV-A light without visible light is needed, particularly in observing fluorescence.

High power mercury vapor black light lamps are made in power ratings of 100 to 1,000 watts.
These do not use phosphors, but rely on the intensified and slightly broadened 350–375 nm spectral line of mercury from high pressure discharge at between 5 and 10 standard atmospheres (500 and 1,000 kPa), depending upon the specific type.
They are more efficient UVA producers per unit of power consumption than fluorescent tubes.

## LEDs

A light-emitting diode (LED) is a semiconductor light source that emits light when current flows through it.
Electrons in the semiconductor recombine with electron holes, releasing energy in the form of photons.

The color of the light (corresponding to the energy of the photons) is determined by the energy required for electrons to cross the band gap of the semiconductor.
Thus, by selection of different semiconductor materials, single-color LEDs can be made that emit light in a narrow band of wavelengths from near-infrared through the visible spectrum and into the ultraviolet range.
As the wavelengths become shorter, because of the larger band gap of these semiconductors, the operating voltage of the LED increases.

With AlGaN and AlGaInN, even shorter wavelengths are achievable.
UV-C wavelengths were obtained in laboratories using aluminium nitride (210 nm), boron nitride (215 nm) and diamond (235 nm).

## Lasers

Another, flight-proven and fully functional solution comes in a form of a laser developed by ESA for the Aeolus satellite mission.

## Ways UV can be used in a Lunar Mission

Research pioneered by NASA shows that UV exposure has multiple desirable effects to lunar dust particles, some of which are highly applicable for the man's return to the moon.
More details will be found in a separate file.

UV exposure leads to changes in the particle's chemistry (further oxidation of iron), which results in changes in both the absorvity and reflectance spectra, the latter being more important, as it can subsequently assist in detecting lunar dust particles by beaming them with radiation of some certain, tuned wavelength.

Additionally, as it was mentioned on the `uv` file, UV can be used to initiate chemical reactions and  also detect fluorescence.
This, coupled with a Teraphtalate Assay (TA) can be applied for both detection and clearing techiniques.

Also, UV exposure can charge the particles and levitate them in low altitudes.
This can be used for particle transfer, as well as for cleaning purposes.
