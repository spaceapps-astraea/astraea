# Basic properties of lunar dust particles
## Diameter
- 55-79 um, but with significant variations (order of ~ 20 um) depending on the sample
- ~20% of total \# of particles smaller than 20 um
- ~10% of total \# of particles smaller than 10 um

## Surface composition and texture
- Angular and jagged surface, consisting mainly of impact-generated glass, 
which contains traces of metallic iron.

## Density
- Ranges from 2.7 to 3 gr/cm3

## Electrical properties
- **Dependent on exposure to solar light**
- Electrical conductivity: 1e(-14) Ω/m (while in darkness)
Increases to 1e(-8) Ω/m when under solar exposure.
Low electrical conductivity => tendency to retain electrical charge for long periods of time!

## Toxicological properties
- Sharp edges => easier transition to blood circulation for particles w/ size <100 nm
- If on circulation, the near-neutral pH of the blood => harder dissolution


# Sources:
- https://www.researchgate.net/publication/323396000\_Lunar\_Dust\_Properties\_and\_Investigation\_Techniques
- https://www.lpi.usra.edu/meetings/nlsc2008/pdf/2072.pdf
