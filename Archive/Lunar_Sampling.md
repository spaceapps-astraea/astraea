# Ways for sample collection (Apollo missions)
- Tongs for rock samples
- Scoops for soil samples
- Rakes for pebbles
- Core tubes for below-surface samples
- Contact soil sampling: beta cloth sampler (for first 100 um), velvet cloth sampler
(for uppermost 1 mm)


# Storage for Earth return (Possibly irrelevant: they are vacuum sealed, not pressurized)
- Apollo Lunar Sample Return Container (ALSRC): Main box containing sample bags.
Preserves a lunar-like vacuum, protects from shocks.
In practice, 4 in 12 ALSRCs returned had leaks due to dust interfering with the seals.
- Core Sample Vacuum Container (CSVC): Has vacuum sealing, not for surface samples.
- Sample bags (rectangular, cup-shaped): Samples were stored inside them, then secured inside
the ALSRC. No mention of vacuum seal.
- Gas Analysis Sample Container (GASC): Reliable vacuum-sealed container. Placed inside a larger container
and upon return to Earth the bottom was punctured to study the lunar atmosphere.
- Special Environmental Sample Container (SESC): Also vacuum-sealed. Used a knife-edge seal into metal
to ensure that the sample inside was not exposed to terrestrial or spacecraft cabin atmosphere.
Useful thing here is that the knife edge and the lid were packed with teflon sheets to prevent dust
from interfering with the seal. The astronaut would remove the seal protectors only when the container was
ready to be closed.

## Sources:
- https://www.lpi.usra.edu/lunar/samples/apollo/tools/
- https://curator.jsc.nasa.gov/lunar/catalogs/other/jsc23454toolcatalog.pdf       
