#### Sources
1. Estimate of safe human exposure levels for lunar dust based oncomparative benchmark dose modeling (doi:10.3109/08958378.2013.777821)
2. https://www.lpi.usra.edu/decadal/leag/DavidJLoftus.pdf
3. https://en.wikipedia.org/wiki/Silicosis#Prevention
4. https://www.elektor.com/grove-dust-concentration-sensor

#### Intro
Inhalation of freshly-cut crystalline silica particles causes _silicosis_ and is believed to be a threat for astronauts of manned missions, since the lunar dust contains a large percantage of silicon (approx. 50% of Si02) [2]. Unfortunately, _silicosis_ is a disease with no known cure [3] and therefore the most effective way ensure the astronauts' safety is to **prevent** their intoxication

#### Ways 
* **Minimization of exposure levels:** A thorough study for the calculation of the human safety exposure to lunar dust can be found in [1]. "**0.5–1 mg/m3** of lunar dust is safe for periodic human exposures during long stays in habitats on the lunar surface". A dust concentration sensor [4] could be used to notify the user whether it is possible to remove their spacesuit. 