## Phases
* **Phase 0**
	* Astrosuit filters with embedeed electrodes
	* Dirt shielding *(Like water repellant coating)*
	* Robot/Drone using TA and UV to target specific areas
* **Phase 1**
	* Lamp with charged plates
	* Integrated solution system on top of the suit that uses TA *(Terephthalic Acid)* & UV
* **Phase 2**
	* UV + Charge coupled with airflow
* **Phase 3**
	* Have it all around the airlock, UV lamps turn on to deactivate particles
	* *Explore hydrophillic/phobic particle properties*
* **Common**
	* On **Phase 0** and **Phase 1** standing wave electric curtain and electrically charged plates can be used.

## Detection + Mitigation + In-situ Analyses
* Sensors *(Outside of the spacecraft, on the suit)*
* Rover/Astronaut uses TA and UV to scan/map
* The amazing ...
* If on lunar base, spectroscopy and more
* Vacuum
* UV + Pulses for particle transfer0
* Data is beamed to Earth for future analysis. *(Two-way communication)*
* Earth analysis
	* VUV
	* Low energy protons
	* Accelerator -> High energy protons
	* TA with better spectrometry
	* EPR *(Paramagnetic Resonance)*
