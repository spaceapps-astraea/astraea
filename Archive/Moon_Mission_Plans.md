## Links and References
1. [Artemis 3](https://en.wikipedia.org/wiki/Artemis_3)
2. [List of Missions](https://en.wikipedia.org/wiki/List_of_missions_to_the_Moon)

## Script
* There are many future missions planned most of which are robotic, non-crewed missions. Among those missions some of them are unsure, based on *number 2*, and they are all planned for 2030 with Lunar landings. One mission that is planned in 2024 by NASA is Artemis 3, found on *number 1*.
