## Common Area
* Note that some of the proposed methods below can be applied in the entrance halway of the spacecraft before entering the hatch, as well as inside the hatch to eliminate the danger of any leftover dust particles on the astranaut's suit. For example the coarse and fine electrical dust removal described below, excluding spray coating, can be applied in the entrance halway.

# Papers and Literature
1. [Controlled particle removal from surfaces byelectrodynamic methods for terrestrial, lunar, andMartian environmental conditions](https://iopscience.iop.org/article/10.1088/1742-6596/142/1/012073/pdf)
2. [Desig of Equipment for Lunar Dust Removal](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19920010136.pdf)
3. [Dust Removal Technology Demonstration for a Lunar Habitat](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100033634.pdf)
4. [Dust Particle Removal by Electrostatic and Dielectrophoretic Forces with Applications to NASA Exploration Missions](http://www.electrostatics.org/images/esa_2008_o1.pdf)
5. [Lotus Plant-Inspired Dust-Busting Shield to Protect Space Gear](https://www.nasa.gov/centers/goddard/news/topstory/2009/lotus_coating.html)
6. [Study of Dust Removal by Standing-Wave Electric Curtain for Application to Solar Cells on Mars](https://www.researchgate.net/publication/224373970_Study_of_Dust_Removal_by_Standing-Wave_Electric_Curtain_for_Application_to_Solar_Cells_on_Mars)

## Ways
* Standing wave curtain
* Brushing, probably using water or similar liquids to enhance the effects of brusing
* Electrically charged plates to attract the dust and remove it from clothes. *(Coarse method)*
* Scanning-like motion with electrically charged plates, on every part of the astronaut's body to attract the dust. *(Finer way, if required)*
* Dust drain by using vacuum flow

## Scripture
- During the entrance of the astronaut in the hatch, as a safety measure, an initial dust removal can be achieved using big charged plates for *coarse* dust removal. After this a finer dust removal in a scanning-like function can be achieved, by using smaller electrically charged plates to scan the hands, legs, main body and generally every part of the astronauts body. Also as a safety measure, after this procedure and if rendered necessary, a spray coating with a color changing, non-destructive, substance can be used to indicate areas of leftover dust.
- Another way to prevent the dust from sticking on the astronaut's suit, is to used a material that has the behavior of the water reppelant coating. A good description of such a material can be found in *number 5*.
