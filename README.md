![alt](Assets/Images/logo.png)
## Abstract
A broadly scoped lunar dust detection, collection, mitigation and analysis system proposal, developed in an effort to protect crew and equipment throughout the whole mission lifecycle, while contributing to our understanding of lunar conditions. Utilizing various novel technologies, and combining them in an integrated, modular, scalable and standardized system/framework, we aim to provide a complete solution to tackle lunar dust issues. Additionally, we explore ways mankind’s understanding can be greatly enhanced. Lunar dust can not only be avoided, but also analyzed and used to improve our life quality if we ever plan to inhabit our Moon.

## Overview
Team Astraea, with the name inspired by the Greek goddess of purity and precision, participates in the NASA Space Apps challenge 2019 organized in Athens, tackling the *"Dust your self off"* challenge. The scope is to provide ways of detection, mapping, mitigation and analysis techniques for lunar dust, a huge issue faced by all missions landing on the lunar surface. The root of the problem is the very nature of the dust being electrically charged thus sticking to almost anything, while also having small particle size. As past missions have shown, dust is an imminent danger for astronauts, causing even death in some cases. All these problems can also be caused by dust in environment other than the moon, like other planets, so the techniques presented here may also apply to other environments too. Through thorough research and combination of various techniques and some creativity, Astraea is proposing ways to detect, collect, mitigate and analyze the lunar dust. Below the table of contents can be found and a more detailed technical presentation follows.

### Table of Contents
- [Future plans](#future-plans)
		- [Further ideas](#further-ideas)
- [Electrostatic repulsion](#electrostatic-repulsion)
	- [Sources](#sources-2)
- [In-situ analysis](#in-situ-analysis)
- [UV](#uv)
	- [Producing UV](#producing-uv)
		- [Lamps](#lamps)
				- [Xenon-Mercury Lamp](#xenon-mercury-lamp)
				- [Blacklight](#blacklight)
	- [LEDs](#leds)
	- [Lasers](#lasers)
	- [Sources](#sources-3)
	- [Ways UV can be used in a Lunar Mission](#ways-uv-can-be-used-in-a-lunar-mission)
		- [Further notes](#further-notes)
- [Lunar sampling](#lunar-sampling)
	- [Ways for sample collection (Apollo missions)](#ways-for-sample-collection-apollo-missions)
	- [Storage for Earth return (Possibly irrelevant: they are vacuum sealed, not pressurized)](#storage-for-earth-return-possibly-irrelevant-they-are-vacuum-sealed-not-pressurized)
	- [Sources:](#sources)
- [Moon mission plans](#moon-mission-plans)
	- [Sources](#sources-4)
- [Preventing lunar dust effects](#preventing-lunar-dust-effects)
	- [Ways](#ways)
	- [Sources](#sources-5)
- [Regolith characteristics](#regolith-characteristics)
	- [Basic properties of lunar dust particles](#basic-properties-of-lunar-dust-particles)
		- [Diameter](#diameter)
		- [Surface composition and texture](#surface-composition-and-texture)
		- [Density](#density)
		- [Electrical properties](#electrical-properties)
		- [Toxicological properties](#toxicological-properties)
	- [Sources:](#sources-1)
- [Removing lunar dust before entering the spacecraft.](#removing-lunar-dust-before-entering-the-spacecraft)
	- [Common Area](#common-area)
	- [Ways](#ways-1)
	- [Sources](#sources-6)
- [Implications for Future Spacesuit Design and Lunar Surface Operations](#implications-for-future-spacesuit-design-and-lunar-surface-operations)
	- [Use of Woven Fabrics](#use-of-woven-fabrics)
	- [Spacesuit Coveralls for Dust Mitigation](#spacesuit-coveralls-for-dust-mitigation)
	- [Spacesuits as a Selective Dust Carrier](#spacesuits-as-a-selective-dust-carrier)
	- [ED-XRF as an Ongoing Dust Assessment Tool](#ed-xrf-as-an-ongoing-dust-assessment-tool)
	- [ED-XRF On-Earth Analysis](#ed-xrf-on-earth-analysis)
	- [Sources](#sources-7)
	- [Additional Sources Used](#additional-sources-used)


## Data handling  
The data produced from the different sensors and instruments of the system will be handled using an integrated sensor network interconnecting its users wirelessly. 

The central point of the network will be the lunar and/or ground station, where a Grafana dashboard will be available for visualization of the acquired data and will allow operators, among others, to analyze measurement results, monitor the concentration of dust particles, get alerts of important events and notify astronauts when their return to the station is deemed nessecary. 

Each of the nodes of the network will feature a lightweight structure based on a Microcontroller Unit (MCU), a transceiver (TRSCVR), a set of sensors and output devices (e.g. UV lamps). The said structure will also be implemented as a wearable device for the astronaut to read real-time data from the sensor network, get instructions on which method of dust mitigation to use, and control/activate the different methods available in each phase through an interactive display.

The proposed network structure offers high modularity and scalability, as new technological advancements regarding lunar dust detection and mitigation can be included in the system as new nodes. 

![alt](Assets/Images/astraea_sens_system_arch.png) 

## Phases
* **Phase 1**
	* Astrosuit fibers with embedded electrodes to repel dust particles
	* Astrosuit with integrated TA & UV for dust particle deactivation in critical parts
	* Suit shielding dust repellent coating, dust-resistant (electrodes) bearings.
	* For prolonged exposure to dust particles (long stay in the surface), analysis must first be conducted by the methods outlined previously. Then, a downstream, more targeted 	removal method should be applied on areas (astrosuit, vehicle, equipment, etc.) with high particle accumulation before proceeding

* **Phase 2**
	* Dust drain with vacuum flow
	* Pair of charged plates to repel/pull particles (coarse)
	* Ramp with smaller charged plates to repel/pull particles  (fine)
	* Standing wave curtain

* **Phase 3**
	* UV + Charge combined with laminar flow

* **Phase 4**
	* Have TA all around the airlock. Xenon-mercury arc lamps activate for about 20~25 minutes to deactivate any dust particles that managed to reach this far into the spacecraft
	* Exploit the hydrophobic properties of the particles by applying aqueous solution either by hand, or by a robotic arm
	* The normal conditions (pressure, oxygen, temperature) of the airlock allow for more and better detection components such as sensors measuring particle concentration in the 	local atmosphere
	* Have it all around the airlock, UV lamps turn on to deactivate particles
	* *Explore hydrophillic/phobic particle properties*


## Sources

---

## Detection + Mitigation + In-situ Analyses

* Sensors *(Outside of the spacecraft, on the suit)*
* Rover/Astronaut uses TA and UV to scan/map
* The amazing kit
* If on lunar base, spectroscopy and more
* Vacuum
* UV + Pulses for particle transfer0
* Data is beamed to Earth for future analysis. *(Two-way communication)*
* Earth analysis
	* VUV
	* Low energy protons
	* Accelerator -> High energy protons
	* TA with better spectrometry
	* EPR *(Paramagnetic Resonance)*

## Sources

---

# Future plans

- Design of a low-cost and affordable, non-invasive field analysis system to study deleterious effects of lunar dust on the respiratory system.
The system includes a lab-on-a-chip platform, where the astronaut can place a sample of his sputum, which will be automatically liquefied and added to the TPA reagent (which operates over a wide range of different pH levels, so it's accessible) to observe fluorescence in dust particles with a spectrometer. 
At the same time, bacteria or apoptotic cells in which the lunar dust has penetrated can be identified and therefore their cellular localization as well as possible accompanying inflammations (bacterial, fungal) can be detected.

The lab-on-a-chip platform will be accompanied with a custom-made imaging system. While a revolutionary approach, we already have constructed a similar microfluidics lab-on-a-chip for cell growth and fluorimetric observations as part of our research (AcubeSAT).

![alt](Assets/Images/microfluidics_chip.png)

This proposed framework can scale very well, and is thus very applicable:
Lunar bases can be established in the future; current designs already exist and are pioneered by NASA.
Lunar bases (perhaps taking Moon colonization into account) provide way greater facilities and resources, thus unlocking the usage of improved analysis (like EPR) methods in-situ.
Looking at the feature, a time where Lunar expeditions commonly happen is a probable scenario. The standardization of the whole procedure and framework will ascertain that research/design teams are on common ground, and ensure increased mission safety.


### Further ideas

- Define the procedure for to be followed when an entity or object re-enters the spacecraft more rigorously, and subsequently make it into a universal standard.
- Construction of hardware prototype of the sensor network and astronaut wearable
- Development of a simple GUI for wearable 
- Gradually model the various components proposed
- Develop a VR/AR demo game where the user can take a look at the prototype complete integrated model suggested, with built-in information available about lunar dust characteristics, as well as the different components and technologies used. Furthermore, the standardized procedure will be available in the form of an interactive “manual”.
- Development of Machine Learning-assisted prediction algorithms that can be ran both on the Ground Stations and on the spacecraft/lunar base to better correlate lunar conditions with dust properties, such as distribution, radiation, charge, dissolution rate, etc.
- The images captured can be sent to Earth for further analysis, where also AI algorithms for classification and data correlation can be used.
- A detailed log will be kept with all the measurements for each crew member. This will not only significantly aid in inventing ways to mitigate lunar dust particles harm caused to humans, but also save lives, as crew members can be sent back home when detecting high dust accumulation.
- The capability to gather data on how lunar dust particles affect humans will significantly increase our insights.



# Electrostatic repulsion

A real world prototype implementation initially used to protect solar panels is pictured below. This application could be expanded and embedded in the astronaut spacesuit, to repell any dust particles as efficiently as possible.

![particle](/Assets/Images/dust_collection.png)

One of the dust removal technology we propose the use of, is based on the electric curtain concept developed by NASA. This technique has been shown to lift and transport charged and uncharged particles using electrostatic and di-electrophoretic forces. The technology has never been applied for space applications on the moon.

![particle](/Assets/Images/dust_particle_hopping.png)

- The Dust Shield consists of a series of parallel electrodes connected to an AC source that generate a traveling wave acting as a contactless conveyor (Fig. 1). Particles are re-pelled by the electrodes used to produce the field and travel along or against the direction of the wave, depending on their polarity. The curtain electrodes can be excited by a sin-gle-phase or a multi-phase AC voltage. In the single-phase electric curtain, parallel cy-lindrical electrodes connected to an AC voltage source generate an electric field whose direction oscillates back and forth as the polarity of the electrodes changes. In this case, a standing wave is produced which would generate a force on any charged particle in the region of the field.
- Since the mesh of electrodes is usually covered with a thin insulating layer to increase the breakdown voltage, uncharged particles falling on the surface before the field is turned on may become charged by repeated contact with the insulating layer, if they bounce, or due to the electrophoretic force, and will be affected by the field once it is turned on. A multi-phase electric curtain produces a traveling wave, since the potential at each electrode changes in steps due to the phase shift. A charged particle in this region will move with or against this wave, depending on its polarity.


## Sources

- [IMPACT OF DUST ON LUNAR EXPLORATION](https://www.nasa.gov/centers/johnson/pdf/486014main_StubbsImpactOnExploration.4075.pdf)
- [Dust Particle Removal by Electrostatic and Dielectrophoretic Forces with Applications to NASA Exploration Missions](http://www.electrostatics.org/images/esa_2008_o1.pdf)
- [Dust mitigation and assessment report](https://www.globalspaceexploration.org/wordpress/docs/Dust%20Mitigation%20Gap%20Assessment%20Report.pdf)
- [Spacesuit](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20090015239.pdf)
- [Solar flares](https://www.nasa.gov/centers/johnson/pdf/486014main_StubbsImpactOnExploration.4075.pdf)
- [NASA Lunar Dust Filtration and Separations Workshop Report](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100004823.pdf)
- [Understanding the Potential Toxic Properties of Lunar Dust](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20090010479.pdf)
- [IMPACT OF DUST ON LUNAR EXPLORATION](https://www.nasa.gov/centers/johnson/pdf/486014main_StubbsImpactOnExploration.4075.pdf)


# In-situ analysis

A dedicated small chamber can be situated inside the spacecraft which will allow larger scale fluorimetric observations, by using more TA, larger, xenon-mercury arc lamps and more potent spectroscopy equipment.
- If on a lunar base, analysis simulating VUV can be achieved, with an increase in energy budget.
- Electron paramagnetic resonance (EPR) spectrometry can be utilized

![alt](Assets/Images/lamps.png)


---

# UV

**Ultraviolet (UV)** is **electromagnetic radiation** with wavelength from 10 nm to 400 nm, present in sunlight.
It is also produced by electric arcs and specialized lights, such as mercury-vapor lamps, or black lights.
Although long-wavelength ultraviolet is not considered an ionizing radiation because its photons lack the energy to ionize atoms, it can cause **chemical reactions** and causes many substances to glow or **fluoresce**. 

Artificially, UV light is usually produced with mercury-vapour lamps.
The trouble is that these lamps also produce a certain visible light content.
However, one can make 'pure' UV of a single wavelength using a laser.
Usually a frequency-doubled argon gas is used to produce a continuous wave beam of UV light.

Generally, if a light source ('the atom') is in an excited state with energy E, it may spontaneously decay to a lower lying level (e.g., the ground state) with energy e, releasing the difference in energy between the two states (E-e) as a photon.
If the difference is energy is just right (between 3.10 eV and 124 eV, the photon produced has the right wavelength (10 nm ~ 400 nm).

## Producing UV

### Lamps

Either Xenon-mercury or blacklight is what we need, alternatives include xenon, mercury, and tanning lamps.

##### Xenon-Mercury Lamp

In xenon-mercury short-arc lamps, the majority of the light is generated in a pinpoint-sized cloud of plasma situated at the tip of each electrode.
The light generation volume is shaped like two intersecting cones, and the luminous intensity falls off exponentially moving towards the centre of the lamp.
Xenon-mercury short-arc lamps have a bluish-white spectrum and extremely high UV output.

The very small size of the arc makes it possible to focus the light from the lamp with moderate precision.
For increased precision, one could use single mode laser diodes and white light supercontinuum lasers that can produce diffraction-limited spot.

All xenon short-arc lamps generate substantial ultraviolet radiation.
Xenon has strong spectral lines in the UV bands, and these readily pass through the fused quartz lamp envelope.
They are also inherently unstable, prone to phenomena such as plasma oscillation and thermal runaway.
Because of that, they require a proper power supply with moderately consistent output.

##### Blacklight

A blacklight emits long-wave (UV-A) ultraviolet light and very little visible light.
Although many other types of lamp emit ultraviolet light with visible light, black lights are essential when UV-A light without visible light is needed, particularly in observing fluorescence.

High power mercury vapor black light lamps are made in power ratings of 100 to 1,000 watts.
These do not use phosphors, but rely on the intensified and slightly broadened 350–375 nm spectral line of mercury from high pressure discharge at between 5 and 10 standard atmospheres (500 and 1,000 kPa), depending upon the specific type.
They are more efficient UVA producers per unit of power consumption than fluorescent tubes.

## LEDs

A light-emitting diode (LED) is a semiconductor light source that emits light when current flows through it.
Electrons in the semiconductor recombine with electron holes, releasing energy in the form of photons.

The color of the light (corresponding to the energy of the photons) is determined by the energy required for electrons to cross the band gap of the semiconductor.
Thus, by selection of different semiconductor materials, single-color LEDs can be made that emit light in a narrow band of wavelengths from near-infrared through the visible spectrum and into the ultraviolet range.
As the wavelengths become shorter, because of the larger band gap of these semiconductors, the operating voltage of the LED increases.

With AlGaN and AlGaInN, even shorter wavelengths are achievable.
UV-C wavelengths were obtained in laboratories using aluminium nitride (210 nm), boron nitride (215 nm) and diamond (235 nm).

## Lasers

Another, flight-proven and fully functional solution comes in a form of a laser developed by ESA for the Aeolus satellite mission.

## Sources

---

## Ways UV can be used in a Lunar Mission

Research pioneered by NASA shows that UV exposure has multiple desirable effects to lunar dust particles, some of which are highly applicable for the man's return to the moon.
More details will be found in a separate file.

UV exposure leads to changes in the particle's chemistry (further oxidation of iron), which results in changes in both the absorvity and reflectance spectra, the latter being more important, as it can subsequently assist in detecting lunar dust particles by beaming them with radiation of some certain, tuned wavelength.

Additionally, as it was mentioned on the `uv` file, UV can be used to initiate chemical reactions and  also detect fluorescence.
This, coupled with a Teraphtalate Assay (TA) can be applied for both detection and clearing techiniques.

Also, UV exposure can charge the particles and levitate them in low altitudes.
This can be used for particle transfer, as well as for cleaning purposes.

### Further notes
- The  low  electrical  conductivity  of  the  regolith  allows  individual  dust  grains  to  retain  electrostatic  charge.  On  the   dayside   conductivity   can   increase   with   surface   temperature  and  infra-red  and  ultra-violet  radiation;  therefore,   this   needs   to   be   taking   into   account   for   surface and dust charging processes.
- Alvarez (1975, 1977b) measured the effects of ultraviolet (UV), visible, and infrared (IR) irradiation on the electrical conductivity of lunar samples, including lunar soils. Relative to their surface electrical conductivity in darkness, the soils show about a $10^1$ increase in conductivity in the IR and a $10^6$ increase in the UV. The latter increase is comparable to that produced by an 800°C increase in temperature. These large changes in conductivity with irradiation can produce large movements of electrical charge across the solar terminator on the lunar surface. [Lunar sourcebook, P532.]()
- The two highest curves were obtained with all three sources on (VIS, IR, UV).


# Lunar sampling

## Ways for sample collection (Apollo missions)
The previous Apollo missions basic techniques that did not include automated methods for lunar soil sampling. Astronauts were needed to come in contact with the soil itself, either to collect dust or to sample it using the specially crafted metal cores, used for further depth.

![fig1](/Assets/Images/core_tube_dust_sampler.png)
![fig1](/Assets/Images/lunar_regolith.png)

- Tongs for rock samples
- Scoops for soil samples
- Rakes for pebbles
- Core tubes for below-surface samples
- Contact soil sampling: beta cloth sampler (for first 100 um), velvet cloth sampler
(for uppermost 1 mm)

## Storage for Earth return (Possibly irrelevant: they are vacuum sealed, not pressurized)
- Apollo Lunar Sample Return Container (ALSRC): Main box containing sample bags.
Preserves a lunar-like vacuum, protects from shocks.
In practice, 4 in 12 ALSRCs returned had leaks due to dust interfering with the seals.
- Core Sample Vacuum Container (CSVC): Has vacuum sealing, not for surface samples.
- Sample bags (rectangular, cup-shaped): Samples were stored inside them, then secured inside
the ALSRC. No mention of vacuum seal.
- Gas Analysis Sample Container (GASC): Reliable vacuum-sealed container. Placed inside a larger container
and upon return to Earth the bottom was punctured to study the lunar atmosphere.
- Special Environmental Sample Container (SESC): Also vacuum-sealed. Used a knife-edge seal into metal
to ensure that the sample inside was not exposed to terrestrial or spacecraft cabin atmosphere.
Useful thing here is that the knife edge and the lid were packed with teflon sheets to prevent dust
from interfering with the seal. The astronaut would remove the seal protectors only when the container was
ready to be closed.

Some particles were also collected from the Apollo airflow system, pictured in the photo below.

![particle](/Assets/Images/apollo_11_air_duct.png)

## Sources:
- [The Chemical Reactivity of Lunar Dust Relevant to Human Exploration of the Moon](https://www.lpi.usra.edu/lunar/samples/apollo/tools/)
- [Catalog og Apollo Lunar Surface Geological Sampling Tools and Containers](https://curator.jsc.nasa.gov/lunar/catalogs/other/jsc23454toolcatalog.pdf) 

---
# Moon mission plans

* There are many future missions planned most of which are robotic, non-crewed missions. Among those missions some of them are unsure, based on *number 2*, and they are all planned for 2030 with Lunar landings. One mission that is planned in 2024 by NASA is Artemis 3, found on *number 1*.

## Sources
1. [Artemis 3](https://en.wikipedia.org/wiki/Artemis_3)
2. [List of Missions](https://en.wikipedia.org/wiki/List_of_missions_to_the_Moon)


---
# Preventing lunar dust effects

Inhalation of freshly-cut crystalline silica particles causes _acute silicosis_ and is believed to be a threat for astronauts of manned missions, since the lunar dust contains a large percantage of silicon (approx. 50% of Si02). Unfortunately, _silicosis_ is a disease with no known cure and therefore the most effective way ensure the astronauts' safety is to **prevent** their intoxication.

## Ways 
* **Minimization of exposure levels:** A thorough study for the calculation of the human safety exposure to lunar dust can be found in. "**0.5–1 mg/m3** of lunar dust is safe for periodic human exposures during long stays in habitats on the lunar surface". A dust concentration sensor could be used to notify the user whether it is possible to remove their spacesuit. 

<!--TODO: find the sensor? Insert image? -->

## Sources
1. Estimate of safe human exposure levels for lunar dust based oncomparative benchmark dose modeling (doi:10.3109/08958378.2013.777821)
2. https://www.lpi.usra.edu/decadal/leag/DavidJLoftus.pdf
3. https://en.wikipedia.org/wiki/Silicosis#Prevention
4. https://www.elektor.com/grove-dust-concentration-sensor

---
# Regolith characteristics

## Basic properties of lunar dust particles

![particle](/Assets/Images/table_of_dust_properties.png)

- *“Lunar regolith”* describes the layer of particles on the Moon’s surface generated by meteoritic impacts, and is similar to terrestrial volcanic ash. The finest component is referred to as dust (<100μm).
- Average  regolith  grain  size  is  ~70μm,  (too  fine  to  see  with the human eye).  The dust component from Apollo samples  has  been  directly  observed  to  contain  grains  as  small  as  0.01μm. Grain  shapes  are  highly  variable  and  can  range  from  spherical  to  extremely  angular;  although, in general, they are somewhat elongated.

![particle](/Assets/Images/dust_particle.png)

### Diameter
- 55-79 um, but with significant variations (order of ~ 20 um) depending on the sample
- ~20% of total \# of particles smaller than 20 um
- ~10% of total \# of particles smaller than 10 um

![particle](/Assets/Images/micrometeorites_particle.png)

### Surface composition and texture
- Angular and jagged surface, consisting mainly of impact-generated glass, 
which contains traces of metallic iron.

### Density
- Ranges from 2.7 to 3 gr/cm3

### Electrical properties
- **Dependent on exposure to solar light**
- Electrical conductivity: 1e(-14) Ω/m (while in darkness)
Increases to 1e(-8) Ω/m when under solar exposure.
Low electrical conductivity => tendency to retain electrical charge for long periods of time!

### Toxicological properties
- Sharp edges => easier transition to blood circulation for particles w/ size <100 nm
- If on circulation, the near-neutral pH of the blood => harder dissolution

## Sources:
- [Lunar Dust: Properties and Investigation Techiniques](https://www.researchgate.net/publication/323396000\_Lunar\_Dust\_Properties\_and\_Investigation\_Techniques)
- [Lunar Dust: Chemistry and Physical Properties and Implications for Toxicity](https://www.lpi.usra.edu/meetings/nlsc2008/pdf/2072.pdf)

---

# Removing lunar dust before entering the spacecraft.

- During the entrance of the astronaut in the hatch, as a safety measure, an initial dust removal can be achieved using big charged plates for *coarse* dust removal. After this a finer dust removal in a scanning-like function can be achieved, by using smaller electrically charged plates to scan the hands, legs, main body and generally every part of the astronauts body. Also as a safety measure, after this procedure and if rendered necessary, a spray coating with a color changing, non-destructive, substance can be used to indicate areas of leftover dust.
- Another way to prevent the dust from sticking on the astronaut's suit, is to used a material that has the behavior of the water reppelant coating. A good description of such a material can be found in *number 5*.

## Common Area
* Note that some of the proposed methods below can be applied in the entrance halway of the spacecraft before entering the hatch, as well as inside the hatch to eliminate the danger of any leftover dust particles on the astranaut's suit. For example the coarse and fine electrical dust removal described below, excluding spray coating, can be applied in the entrance halway.

## Ways
* Standing wave curtain
* Brushing, probably using water or similar liquids to enhance the effects of brusing
* Electrically charged plates to attract the dust and remove it from clothes. *(Coarse method)*
* Scanning-like motion with electrically charged plates, on every part of the astronaut's body to attract the dust. *(Finer way, if required)*
* Dust drain by using vacuum flow.

## Sources
1. [Controlled particle removal from surfaces byelectrodynamic methods for terrestrial, lunar, andMartian environmental conditions](https://iopscience.iop.org/article/10.1088/1742-6596/142/1/012073/pdf)
2. [Desig of Equipment for Lunar Dust Removal](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19920010136.pdf)
3. [Dust Removal Technology Demonstration for a Lunar Habitat](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100033634.pdf)
4. [Dust Particle Removal by Electrostatic and Dielectrophoretic Forces with Applications to NASA Exploration Missions](http://www.electrostatics.org/images/esa_2008_o1.pdf)
5. [Lotus Plant-Inspired Dust-Busting Shield to Protect Space Gear](https://www.nasa.gov/centers/goddard/news/topstory/2009/lotus_coating.html)
6. [Study of Dust Removal by Standing-Wave Electric Curtain for Application to Solar Cells on Mars](https://www.researchgate.net/publication/224373970_Study_of_Dust_Removal_by_Standing-Wave_Electric_Curtain_for_Application_to_Solar_Cells_on_Mars)

---
# Implications for Future Spacesuit Design and Lunar Surface Operations 

## Use of Woven Fabrics

Woven fabrics were the foundation for the functional design of the Apollo spacesuits. Even 35 years after the lunar missions, these fabrics were found to contain a significant contamination level of lunar dust particles. There is evidence that retention of these contaminating particles on the spacesuit is promoted by interaction between the dust particles and the weave of the spacesuit fabrics. It follows by implication that alternatives to woven fabrics should probably be found for future spacesuits. If future woven fabrics are employed, they should employ appropriate surface coatings to keep particles from entering and penetrat-ing the fabric weave. As might be expected, we have also found that fabric wear and suspectibility to lu-nar dust contamination go hand-in-hand, implying that toughness and wear resistance of any future space-suit fabrics will be essential to providing dust resistance.

## Spacesuit Coveralls for Dust Mitigation

Current discussions of ways to mitigate dust effects on spacesuits over extended lunar missions have considered employing a one-time-use lightweight coverall garment to protect spacesuit outer systems and surfaces. Our findings for the distribution of lunar dust on the Apollo spacesuits show normal EVA opera-tions will result in even the uppermost parts of the spacesuit, including the arms, becoming exposed to high levels of soil contamination. Therefore, a partial cover garment protecting only the lower half of the spacesuit will not be sufficient, and any successful design must cover the entire spacesuit, including the arms.

## Spacesuits as a Selective Dust Carrier

To the extent that the different mineralogical components of the lunar soil may ultimately be shown to have different levels of toxic threat to lunar crews, or physical threats to lunar surface systems, the composition of lunar soil at a given exploration site may be a factor in assessing risk factors for various mission scenarios. Our findings clearly show that the soil composition at a given mission site is not nec-essarily what will be brought into a spacecraft or habitat environment by the spacesuit after a given EVA. Future spacesuits will need to be tested with respect to their selective carrying capacity for different lunar soil components so that contamination risks to the spacecraft environment can be adequately assessed.

## ED-XRF as an Ongoing Dust Assessment Tool

We successfully used ED-XRF as a tool to make an indirect assessment of the amount of lunar soil la-tent on the Apollo 17 spacesuit. Once properly calibrated, the technique provides rapid results and is eas-ily portable. This technology should be evaluated further for applications where dust contamination isbeing evaluated in lunar analog-based systems testing or on the lunar surface itself.  Performance of Apollo-Era Rotating Pressure Seals in the Lunar Dust Environment We found no evidence that the sealed 360° wrist rotation bearing on the EV pressure glove was com-promised by lunar dust. It is quite possible that this Apollo-era design may be a useful baseline for de-signing dust-resistance rotating pressure seals for future lunar surface sys


## ED-XRF On-Earth Analysis

<!-- TODO: add image of suit points of interest and disciss about the on-suit sensors in depth -->
<!-- Also make this better / review it. -->
![particle](/Assets/Images/spacesuit_points_of_interest.png)

The ED-XRF analyses uncovered a significant range of variation in the concentration of Ca(2500-17,000 ppm), Ti (240-6,300 ppm) and Fe (850-10,000 ppm), strongly suggesting that these three elements are significantly linked to the amount of lunar soil contamination on the spacesuit fabric. How-ever, as noted above, of the three elements, Ti has the lowest background in the un-contaminated fabric and is therefore likely to be the more sensitive chemical marker to use as a proportionate measure of the amount of latent lunar soil in the fabric. This hypothesis was further confirmed based on chemical varia-tion plots for the entire set of analyses which showed that when the relative ratios of Ca/Ti and Fe/Ti were each plotted against absolute Ti, both ratios reach relatively constant “plateau” levels at very low values of absolute Ti. The plateau value in this case likely represents the intrinsic element ratios of the lunar soil contaminant, and the relationships in the plots are consistent with a very low intrinsic background for Ti. 

Using Ti as a compositional marker for lunar soil contamination, Figure 24shows maps of the Ti concentrations corresponding to the tape-sample locations on the front and back of the Apollo 17 ITMG.The concentration values are given in ppm x $10^2$ based on one significant figure precision. There is over-all excellent correlation between the Ti concentration numbers and areas of the suit that appear substantially more “dirty” based on visual inspection. These areas include the lower legs and lower back, both portions of the suit where a high amount of contact with soil would be expected. The correlation is addi-tionally confirmed if the fabric gray scale color values for several of the sampled locations are plotted against measured Ti concentration. Although there is some spread in the Ti concentration values at each gray level, a general positive trend is apparent. The overall results confirm ED-XRF as a potentially useful tool for measuring lunar soil contamination on other spacesuits, and in other applications in-volving fabrics, or laminated materials such as carbon composites.

**The analyses showcased that the areas around the knees were most likely to contain the most amound of dust. Other areas of interest include the sholders and legs.**

**Based on systematic sampling of particles from the outermost fabric surface of the Apollo 17 LMP ITMG using an adhesive tape extraction method combined with SEM examination, the mean grain size of lunar soil particles on the fabric was determined to be less than 10.4 μm**


## Sources

---

## Additional Sources Used
* [Lunar Dust and Lunar Simulant Activation and Monitoring](https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1945-5100.2009.tb00781.x)
* [Lunar Sourcebook](https://www.lpi.usra.edu/publications/books/lunar_sourcebook/)
* [Lunar Dust Charging by Photoelectric Emissions](https://www.sciencedirect.com/science/article/abs/pii/S003206330600359X)
* [Understanding the Activation and Solution Properties of Lunar Dust for Future Lunar Habitation](https://www.researchgate.net/publication/253191712_Understanding_the_Activation_and_Solution_Properties_of_Lunar_Dust_for_Future_Lunar_Habitation)
* [A high-throughput assay for enzymatic polyester hydrolysis activity by fluorimetric detection](https://www.ncbi.nlm.nih.gov/pubmed/22623363)
* [Scientists Developing Ways to Mitigate Dust Problems for Explorers](https://www.nasa.gov/content/scientists-developing-ways-to-mitigate-dust-problem-for-explorers)
* [Lunar Dust Devitation](https://www.researchgate.net/publication/245307656_Lunar_Dust_Levitation)
* [Variability of the lunar photoelectron sheath and dust mobility due to solar activity](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2008JA013487)
* [Xenon arc lamp spectral radiance modelling for satellite instrument calibration](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/9904/99044V/Xenon-arc-lamp-spectral-radiance-modelling-for-satellite-instrument-calibration/10.1117/12.2232299.short?SSO=1)
* [ADM-Aeolus Wikipedia Page](https://en.wikipedia.org/wiki/ADM-Aeolus#Scientific_payload)
* [Electron Paramagnetic Resonance Wikipedia Page](https://en.wikipedia.org/wiki/Electron_paramagnetic_resonance)
* [Electron Paramagnetic Resonance (book)](https://books.google.gr/books?hl=en&lr=&id=X7M4BQAAQBAJ&oi=fnd&pg=PP1&dq=electron+paramagnetic+resonance&ots=VTcOy8_H6u&sig=iwCB34pN4-tpQljVyGde6IKMeY0&redir_esc=y#v=onepage&q=electron%20paramagnetic%20resonance&f=false)
